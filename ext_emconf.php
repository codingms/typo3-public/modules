<?php

$EM_CONF['modules'] = [
    'title' => 'Modules',
    'description' => 'Modules - Little helper for creating frontend and backend modules in TYPO3',
    'category' => 'be',
    'author' => 'Thomas Deuling',
    'author_email' => 'typo3@coding.ms',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '7.0.1',
    'constraints' => [
        'depends' => [
            'php' => '8.1.0-8.3.99',
            'typo3' => '12.4.0-13.4.99',
            'additional_tca' => '1.16.3-1.99.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
