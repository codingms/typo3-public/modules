# Modules Erweiterung für TYPO3

Kleine Helfer für TYPO3-Erweiterungs-Entwickler zum Erstellen von Backend-Modulen und Admin-Bereichen im Frontend in TYPO3.


**Features:**

*   Profilverarbeitung für eingeloggte Frontend-Benutzer
*   Registrierung für Frontend-Benutzer (optional: Registrierungsbestätigung durch Site-Administrator)
*   Einladungscodes für die Registrierung (personalisierbar und zeitliche Begrenzung möglich)
*   Backendmodul für die Verwaltung von Frontend-Benutzern inkl. Autorisationen
*   Backendmodul für die Verwaltung von Backend-Benutzern (auch einsetzbar für Redakteure) inkl. Autorisationen
*   Backendmodul für die Verwaltung von Einladungscodes für die Registrierung
*   Backendmodul für den Import von Einladungscodes für die Registrierung
*   Toolbox für die Erstellung von Backendmodulen (Ein Beispiel in ist der EXT:internal_notes im TYPO3-TER zu finden)
*   Toolbox für die Erstellung von Frontend-Administrations-Bereichen
*   Toolbox für die Erstellung von umfangreichen Intranet und Applikationen
*   Frontend-Benutzer Liste mit Detailansicht im Frontend, zum Anzeigen deines Teams oder Benutzer

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Modules Dokumentation](https://www.coding.ms/documentation/typo3-modules "Modules Dokumentation")
*   [Modules Bug-Tracker](https://gitlab.com/codingms/typo3-public/modules/-/issues "Modules Bug-Tracker")
*   [Modules Repository](https://gitlab.com/codingms/typo3-public/modules "Modules Repository")
*   [TYPO3 Modules Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-modules/ "TYPO3 Modules Produktdetails")
*   [TYPO3 Modules Dokumentation](https://www.coding.ms/de/dokumentation/typo3-modules/ "TYPO3 Modules Dokumentation")
*   [TYPO3 Modules Download](https://typo3.org/extensions/repository/view/modules "TYPO3 Modules Download")
