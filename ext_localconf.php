<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Modules',
    'Registration',
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'registration,invitationCodeAvailable'],
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'registration,invitationCodeAvailable'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Modules',
    'Profile',
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'profile'],
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'profile'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Modules',
    'LoginFromBackend',
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'loginFromBackend'],
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'loginFromBackend']
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Modules',
    'Listing',
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'list,show'],
    [\CodingMs\Modules\Controller\FrontendUserController::class => 'list,show'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
//
// Allow backend users to drag and drop the new page type:
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 13) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
        "@import 'EXT:modules/Configuration/user.tsconfig'"
    );
}
//
// Backend TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
    '@import "EXT:modules/Configuration/TypoScript/Backend/setup.typoscript"'
);
//
// Handle authorizations
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['checkModifyAccessList']['modules'] =
    \CodingMs\Modules\Hook\DataHandlerCheckModifyAccessListHook::class;
//
// Override Mail template paths
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1659186453] = 'EXT:modules/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][1659186453] = 'EXT:modules/Resources/Private/Layouts/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1659186453] = 'EXT:modules/Resources/Private/Partials/Email/';
