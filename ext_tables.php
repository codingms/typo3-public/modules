<?php

use TYPO3\CMS\Core\DataHandling\PageDoktypeRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
$dokTypeRegistry = GeneralUtility::makeInstance(PageDoktypeRegistry::class);
$dokTypeRegistry->add(
    1659186453,
    [
        'type' => 'web',
        'allowedTables' => implode(
            ',',
            [
                'fe_users',
                'fe_groups',
                'sys_file_reference',
                'pages',
            ]
        ),
    ]
);
//
// Authorizations in backend
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_backend_user_action_permissions'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Modules\Domain\DataTransferObject\BackendUserActionPermission::class
    );
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_frontend_user'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Modules\Domain\DataTransferObject\FrontendUserActionPermission::class
    );
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_frontend_user_group'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Modules\Domain\DataTransferObject\FrontendUserGroupActionPermission::class
    );
$GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_frontend_user_invitation_code'] =
    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \CodingMs\Modules\Domain\DataTransferObject\FrontendUserInvitationCodeActionPermission::class
    );
