<?php

namespace CodingMs\Modules\Event\Backend;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class ManipulateExportRecordEvent
{
    /**
     * @var array<string, mixed>
     */
    protected array $array = [];
    protected int $uid = 0;
    protected string $listId = '';

    /**
     * @param array<string, mixed> $array
     * @param int $uid
     */
    public function __construct(array $array, int $uid, string $listId)
    {
        $this->array = $array;
        $this->uid = $uid;
        $this->listId = $listId;
    }

    /**
     * @return array<string, mixed>
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * @param array<string, mixed> $array
     */
    public function setArray(array $array): void
    {
        $this->array = $array;
    }

    public function getUid(): int
    {
        return $this->uid;
    }

    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    public function getListId(): string
    {
        return $this->listId;
    }

    public function setListId(string $listId): void
    {
        $this->listId = $listId;
    }
}
