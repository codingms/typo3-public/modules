<?php

namespace CodingMs\Modules\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\AddressTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CityTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CompanyTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CountryStringTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CreationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\DisableTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\EmailTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\EndtimeTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\FirstNameTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\GenderTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\LastNameTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ModificationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\NameTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\StarttimeTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TelephoneTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\UsernameTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\VatNumberTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ZipTrait;
use CodingMs\Modules\Domain\Model\Traits\CheckMethodTrait;
use CodingMs\Modules\Domain\Model\Traits\ToCsvArrayTrait;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity as FrontendUserParent;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Frontend user
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class FrontendUser extends FrontendUserParent
{
    use CheckMethodTrait;
    use ToCsvArrayTrait;
    use CreationDateTrait;
    use ModificationDateTrait;
    use DisableTrait;
    use StarttimeTrait;
    use EndtimeTrait;
    use CompanyTrait;
    use EmailTrait;
    use TitleTrait;
    use GenderTrait;
    use NameTrait;
    use FirstNameTrait;
    use LastNameTrait;
    use AddressTrait;
    use ZipTrait;
    use CityTrait;
    use VatNumberTrait;
    use TelephoneTrait;
    use CountryStringTrait;
    use UsernameTrait;
    use DescriptionTrait;

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var ObjectStorage<FrontendUserGroup>
     */
    protected $usergroup;

    /**
     * @var string
     */
    protected $middleName = '';

    /**
     * @var string
     */
    protected $fax = '';

    /**
     * @var string
     */
    protected $www = '';

    /**
     * @var ObjectStorage<FileReference>
     */
    protected $image;

    /**
     * @var \DateTime|null
     */
    protected $lastlogin;

    /**
     * @var \DateTime
     */
    protected $birthday;

    /**
     * @var string
     */
    protected $mobile = '';

    /**
     * @var string
     */
    protected $hash = '';

    /**
     * @var bool
     */
    protected $termsConfirmed = false;

    /**
     * @var bool
     */
    protected $privacyConfirmed = false;

    /**
     * @var bool
     */
    protected $disclaimerConfirmed = false;

    /**
     * @var bool
     */
    protected $newsletter = false;

    /**
     * @var string
     */
    protected $profession = '';

    /**
     * @var string
     */
    protected $maritalStatus = '';

    /**
     * @var int
     */
    protected $children = 0;

    /**
     * @var string
     */
    protected $recordType = '';

    /**
     * @var string
     */
    protected $bankAccountOwnerName = '';

    /**
     * @var string
     */
    protected $bankAccountBankName = '';

    /**
     * @var string
     */
    protected $bankAccountBic = '';

    /**
     * @var string
     */
    protected $bankAccountIban = '';

    /**
     * @var string
     */
    protected $accountingType = '';

    /**
     * @var string
     */
    protected $jobTitle = '';

    /**
     * @var string
     */
    protected $vita = '';

    /**
     * Constructs a new Front-End User
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username = '', $password = '')
    {
        $this->username = $username;
        $this->password = $password;
        $this->usergroup = new ObjectStorage();
        $this->image = new ObjectStorage();
    }

    /**
     * Called again with initialize object, as fetching an entity from the DB does not use the constructor
     */
    public function initializeObject()
    {
        $this->usergroup = $this->usergroup ?? new ObjectStorage();
        $this->image = $this->image ?? new ObjectStorage();
    }

    /**
     * Sets the password value
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the password value
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the usergroups. Keep in mind that the property is called "usergroup"
     * although it can hold several usergroups.
     *
     * @param ObjectStorage<FrontendUserGroup> $usergroup
     */
    public function setUsergroup(ObjectStorage $usergroup)
    {
        $this->usergroup = $usergroup;
    }

    /**
     * Adds a usergroup to the frontend user
     *
     * @param FrontendUserGroup $usergroup
     */
    public function addUsergroup(FrontendUserGroup $usergroup)
    {
        $this->usergroup->attach($usergroup);
    }

    /**
     * Removes a usergroup from the frontend user
     *
     * @param FrontendUserGroup $usergroup
     */
    public function removeUsergroup(FrontendUserGroup $usergroup)
    {
        $this->usergroup->detach($usergroup);
    }

    /**
     * Returns the usergroups. Keep in mind that the property is called "usergroup"
     * although it can hold several usergroups.
     *
     * @return ObjectStorage<FrontendUserGroup> An object storage containing the usergroup
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }

    /**
     * Sets the middleName value
     *
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * Returns the middleName value
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Sets the fax value
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the fax value
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the www value
     *
     * @param string $www
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

    /**
     * Returns the www value
     *
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Sets the image value
     *
     * @param ObjectStorage<FileReference> $image
     */
    public function setImage(ObjectStorage $image)
    {
        $this->image = $image;
    }

    /**
     * Gets the image value
     *
     * @return ObjectStorage<FileReference>
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the lastlogin value
     *
     * @param \DateTime $lastlogin
     */
    public function setLastlogin(\DateTime $lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }

    /**
     * Returns the lastlogin value
     *
     * @return \DateTime
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @return array<string, string>
     */
    public function getEmailNameArray(): array
    {
        return [$this->getEmail() => $this->getName()];
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function getBirthdayAsString()
    {
        if (isset($this->birthday)) {
            return $this->birthday->format('Y-m-d H:i:s');
        }
        return '';
    }

    /**
     * @return int
     */
    public function getYearOfBirth()
    {
        $year = (int)date('Y');
        if ($this->birthday instanceof \DateTime) {
            $year = (int)$this->birthday->format('Y');
        }
        return $year;
    }

    /**
     * @param mixed $birthday
     * @throws \Exception
     */
    public function setBirthday($birthday)
    {
        if (is_string($birthday)) {
            // YYYY-MM-DD
            $dateValues = explode('-', $birthday);
            $year = (int)$dateValues[0];
            $month = (int)$dateValues[1];
            $day = (int)$dateValues[2];
            $valueTimestamp = mktime(0, 0, 0, $month, $day, $year);
            if (!($this->birthday instanceof \DateTime)) {
                $this->birthday = new \DateTime();
            }
            $this->birthday->setTimestamp($valueTimestamp);
        } elseif ($birthday instanceof \DateTime) {
            $this->birthday = $birthday;
        } else {
            throw new \Exception('Unknown birthday format!');
        }
    }

    /**
     * @param int $year
     * @throws \Exception
     */
    public function setYearOfBirth($year)
    {
        $valueTimestamp = mktime(0, 0, 0, 1, 1, $year);
        if (!($this->birthday instanceof \DateTime)) {
            $this->birthday = new \DateTime();
        }
        $this->birthday->setTimestamp($valueTimestamp);
    }

    /**
     * Returns the age of the frontend user
     * @return int
     * @throws \Exception
     */
    public function getAge()
    {
        $age = 0;
        if ($this->birthday instanceof \DateTime) {
            $now = new \DateTime();
            $difference = $now->diff($this->birthday);
            $age = $difference->y;
        }
        return $age;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return bool
     */
    public function getTermsConfirmed()
    {
        return $this->termsConfirmed;
    }

    /**
     * @return bool
     */
    public function isTermsConfirmed()
    {
        return $this->termsConfirmed;
    }

    /**
     * @param bool $termsConfirmed
     */
    public function setTermsConfirmed($termsConfirmed)
    {
        $this->termsConfirmed = $termsConfirmed;
    }

    /**
     * @return bool
     */
    public function getPrivacyConfirmed()
    {
        return $this->privacyConfirmed;
    }

    /**
     * @return bool
     */
    public function isPrivacyConfirmed()
    {
        return $this->privacyConfirmed;
    }

    /**
     * @param bool $privacyConfirmed
     */
    public function setPrivacyConfirmed($privacyConfirmed)
    {
        $this->privacyConfirmed = $privacyConfirmed;
    }

    /**
     * @return bool
     */
    public function getDisclaimerConfirmed()
    {
        return $this->disclaimerConfirmed;
    }

    /**
     * @return bool
     */
    public function isDisclaimerConfirmed()
    {
        return $this->disclaimerConfirmed;
    }

    /**
     * @param bool $disclaimerConfirmed
     */
    public function setDisclaimerConfirmed($disclaimerConfirmed)
    {
        $this->disclaimerConfirmed = $disclaimerConfirmed;
    }

    /**
     * @return bool
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @return bool
     */
    public function isNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * @return string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * @param string $maritalStatus
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
    }

    /**
     * @return int
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param int $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getRecordType(): string
    {
        return $this->recordType;
    }

    /**
     * @param string $recordType
     */
    public function setRecordType(string $recordType): void
    {
        $this->recordType = $recordType;
    }

    /**
     * @return string
     */
    public function getBankAccountOwnerName(): string
    {
        return $this->bankAccountOwnerName;
    }

    /**
     * @param string $bankAccountOwnerName
     */
    public function setBankAccountOwnerName(string $bankAccountOwnerName): void
    {
        $this->bankAccountOwnerName = $bankAccountOwnerName;
    }

    /**
     * @return string
     */
    public function getBankAccountBankName(): string
    {
        return $this->bankAccountBankName;
    }

    /**
     * @param string $bankAccountBankName
     */
    public function setBankAccountBankName(string $bankAccountBankName): void
    {
        $this->bankAccountBankName = $bankAccountBankName;
    }

    /**
     * @return string
     */
    public function getBankAccountBic(): string
    {
        return $this->bankAccountBic;
    }

    /**
     * @param string $bankAccountBic
     */
    public function setBankAccountBic(string $bankAccountBic): void
    {
        $this->bankAccountBic = $bankAccountBic;
    }

    /**
     * @return string
     */
    public function getBankAccountIban(): string
    {
        return $this->bankAccountIban;
    }

    /**
     * @param string $bankAccountIban
     */
    public function setBankAccountIban(string $bankAccountIban): void
    {
        $this->bankAccountIban = $bankAccountIban;
    }

    /**
     * @return string
     */
    public function getAccountingType(): string
    {
        return $this->accountingType;
    }

    /**
     * @param string $accountingType
     */
    public function setAccountingType(string $accountingType): void
    {
        $this->accountingType = $accountingType;
    }

    /**
     * @return string
     */
    public function getJobTitle(): string
    {
        return $this->jobTitle;
    }

    /**
     * @param string $jobTitle
     */
    public function setJobTitle(string $jobTitle): void
    {
        $this->jobTitle = $jobTitle;
    }

    /**
     * @return string
     */
    public function getVita(): string
    {
        return $this->vita;
    }

    /**
     * @param string $vita
     */
    public function setVita(string $vita): void
    {
        $this->vita = $vita;
    }
}
