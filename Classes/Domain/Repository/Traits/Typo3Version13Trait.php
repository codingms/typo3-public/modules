<?php

declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace CodingMs\Modules\Domain\Repository\Traits;

use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Mark this class deprecated once the support for TYPO3 v12 is dropped.
 */
trait Typo3Version13Trait
{
    protected static function getPdoIntParam(): mixed
    {
        if ((int)(VersionNumberUtility::getCurrentTypo3Version()) >= 13) {
            return \Doctrine\DBAL\ParameterType::INTEGER;
        }
        return \PDO::PARAM_INT;
    }
}
