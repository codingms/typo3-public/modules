<?php

namespace CodingMs\Modules\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Frontend user repository
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class FrontendUserRepository extends Repository
{
    /**
     * @param string $username
     * @param string $disable
     * @return object
     */
    public function findByUsername(string $username, string $disable = 'no')
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraints[] = $query->equals('username', $username);
        if ($disable === 'no') {
            $constraints[] = $query->equals('disable', 0);
        } elseif ($disable === 'yes') {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $constraints[] = $query->equals('disable', 1);
        } elseif ($disable === 'both') {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
        }
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        return $query->execute()->getFirst();
    }

    /**
     * @param string $hash
     * @param string $disable
     * @return object
     */
    public function findByHash(string $hash, string $disable = 'no')
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraints[] = $query->equals('hash', $hash);
        if ($disable === 'no') {
            $constraints[] = $query->equals('disable', 0);
        } elseif ($disable === 'yes') {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
            $constraints[] = $query->equals('disable', 1);
        } elseif ($disable === 'both') {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
        }
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        return $query->execute()->getFirst();
    }

    /**
     * @param array $filter
     * @param bool $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = [];
        if (isset($filter['searchWord']) && $filter['searchWord'] !== '') {
            $constraintsSearchWord = [];
            $constraintsSearchWord[] = $query->like('username', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('name', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('email', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$constraintsSearchWord);
        }
        //
        // Backend user can have a frontend user group selection/restriction
        $allowedGroups = $GLOBALS['BE_USER']->user['fe_groups'] ?? '';
        $allowedGroups = GeneralUtility::trimExplode(',', $allowedGroups, true);
        if (isset($filter['usergroup']['selected']) && (int)$filter['usergroup']['selected'] > 0) {
            $constraints[] = $query->contains('usergroup', $filter['usergroup']['selected']);
        } elseif (count($allowedGroups) > 0) {
            $constraintsUserGroup = [];
            foreach ($allowedGroups as $allowedGroup) {
                $constraintsUserGroup[] = $query->contains('usergroup', $allowedGroup);
            }
            $constraints[] = $query->logicalOr(...$constraintsUserGroup);
        }
        if (isset($filter['disabled']) && $filter['disabled']) {
            $constraints[] = $query->equals('disable', '1');
        } else {
            $constraints[] = $query->equals('disable', '0');
        }
        $constraints[] = $query->equals('pid', $filter['pid']);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param array $filter
     * @param bool $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForFrontendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $constraints = [];
        if (isset($filter['searchWord']) && $filter['searchWord'] !== '') {
            $constraintsSearchWord = [];
            $constraintsSearchWord[] = $query->like('username', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('name', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('email', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$constraintsSearchWord);
        }
        //
        if (isset($filter['usergroup']['selected']) && (int)$filter['usergroup']['selected'] > 0) {
            $constraints[] = $query->contains('usergroup', $filter['usergroup']['selected']);
        } elseif (!empty($filter['usergroup']['items'])) {
            $constraints[] = $query->in('usergroup', array_keys($filter['usergroup']['items']));
        } else {
            $constraints[] = $query->equals('usergroup', 0);
        }
        $constraints[] = $query->equals('pid', $filter['pid']);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if (isset($filter['limit']) && isset($filter['offset']) && (int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param int $pid
     * @param int $uid
     * @param array $groups
     * @return object|null
     * @throws InvalidQueryException
     */
    public function findByUidAndGroups(int $pid, int $uid, array $groups = []): ?object
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraints[] = $query->equals('pid', $pid);
        $constraints[] = $query->equals('uid', $uid);
        if (!empty($groups)) {
            $constraintsOr = [];
            foreach ($groups as $id) {
                $constraintsOr[] = $query->contains('usergroup', $id);
            }
            $constraints[] = $query->logicalOr(...$constraintsOr);
        }
        $query->matching(
            $query->logicalAnd(...$constraints)
        );
        return $query->execute()->getFirst();
    }

    /**
     * @param string $name
     * @return array|object[]|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function searchByName(string $name)
    {
        $query = $this->createQuery();
        $where = $query->logicalOr(
            $query->like('first_name', '%' . $name . '%'),
            $query->like('last_name', '%' . $name . '%'),
            $query->like('username', '%' . $name . '%')
        );
        $query->matching($where);
        $query->setOrderings(['uid' => QueryInterface::ORDER_ASCENDING]);
        return $query->execute();
    }

    /**
     * @param string $email
     * @return array|object[]|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function searchByEmail(string $email)
    {
        $query = $this->createQuery();
        $query->matching($query->like('email', '%' . $email . '%'));
        $query->setOrderings(['uid' => QueryInterface::ORDER_ASCENDING]);
        return $query->execute();
    }

    /**
     * @param string $name
     * @param string $email
     * @return array|object[]|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function searchByNameOrEmail(string $name = '', string $email = '')
    {
        $query = $this->createQuery();
        $whereParts = [];
        if (!empty($name)) {
            $whereParts[] = $query->logicalOr(
                $query->like('first_name', '%' . $name . '%'),
                $query->like('last_name', '%' . $name . '%'),
                $query->like('username', '%' . $name . '%')
            );
        }
        if (!empty($email)) {
            $whereParts[] = $query->logicalOr(
                $query->like('email', '%' . $email . '%'),
                $query->like('username', '%' . $email . '%')
            );
        }
        if (count($whereParts) > 0) {
            $where = $query->logicalAnd(...$whereParts);
        }
        if (isset($where)) {
            $query->matching($where);
        }
        $query->setOrderings(['uid' => QueryInterface::ORDER_ASCENDING]);
        return $query->execute();
    }
}
