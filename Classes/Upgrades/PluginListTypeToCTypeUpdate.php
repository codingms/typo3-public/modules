<?php

declare(strict_types=1);

namespace CodingMs\Modules\Upgrades;

use CodingMs\AdditionalTca\Upgrades\AbstractListTypeToCTypeV12V13;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('modulesPluginListTypeToCTypeUpdate')]
final class PluginListTypeToCTypeUpdate extends AbstractListTypeToCTypeV12V13
{
    protected function getListTypeToCTypeMapping(): array
    {
        return [
            'modules_registration' => 'modules_registration',
            'modules_profile' => 'modules_profile',
            'modules_listing' => 'modules_listing',
        ];
    }

    public function getTitle(): string
    {
        return 'PluginListTypeToCTypeUpdate Upgrade Wizard';
    }

    public function getDescription(): string
    {
        return 'EXT:modules: Switches ListTypes to CTypes for TYPO3 V12 and V13';
    }
}
