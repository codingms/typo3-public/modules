<?php

namespace CodingMs\Modules\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Utility\BackendListUtility;
use Exception;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;
use TYPO3\CMS\Backend\Routing\UriBuilder as UriBuilderBackend;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Imaging\IconSize;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Backend base controller
 */
class BackendController extends ActionController
{
    protected IconFactory $iconFactory;
    protected ModuleTemplate $moduleTemplate;
    protected int $pageUid = 0;

    /**
     * @var array<mixed>
     */
    protected array $page = [];

    /**
     * Extension name must be overridden in child classes!
     * @var string
     */
    protected string $extensionName = 'Modules';

    /**
     * Example up-to v11:
     * web_OpenimmoProOpenimmo
     * web_AddressManagerAddressmanager
     *
     * since v12
     * modules_frontenduser
     *
     * @var string
     */
    protected string $moduleName = 'modules_frontenduser';

    /**
     * Example:
     * tx_openimmopro_web_openimmoproopenimmo
     * tx_addressmanager_web_addressmanager
     *
     * @var string
     */
    protected string $modulePrefix = 'tx_modules_web_modulesfrontenduser';

    public function __construct(
        protected TypoScriptService $typoScriptService,
        protected BackendListUtility $backendListUtility,
        protected UriBuilderBackend $uriBuilderBackend,
        protected ModuleTemplateFactory $moduleTemplateFactory
    ) {
        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $this->iconFactory = $iconFactory;
    }

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @throws Exception
     * */
    protected function initializeAction(): void
    {
        $this->pageUid = intval($this->request->getQueryParams()['id'] ?? 0);
        $this->page = BackendUtility::getRecord('pages', $this->pageUid) ?? [];
        //
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile('EXT:modules/Resources/Public/Stylesheets/Modules.css');
        $this->moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        $this->moduleTemplate->setTitle('EXT:modules');
        if (!empty($this->page)) {
            $this->setMetaInformation('pages', $this->pageUid);
        }

        //        //
        //        /** @var BackendTemplateView $view */
        //        if ($this->view->getModuleTemplate() !== null) {
        //            // Include Stylesheets
        //            $extRealPath = PathUtility::getAbsoluteWebPath('../typo3conf/ext/modules/');
        //            // Include JavaScripts
        //            $contribPath = 'Resources/Public/Contrib/';
        //            $contribPath = ExtensionManagementUtility::extPath('modules', $contribPath);
        //
        //            $pageRenderer->addRequireJsConfiguration([
        //                'paths' => [
        //                    'bootstrap-select' => $extRealPath . 'Resources/Public/Contrib/BootstrapSelect/js/bootstrap-select.min',
        //                ],
        //                'bootstrap' => [
        //                    'bootstrap-select' => ['jquery', 'bootstrap'],
        //                ],
        //                'shim' => [
        //                    'bootstrap-select' => [
        //                        'deps' => ['bootstrap'],
        //                    ],
        //                ],
        //            ]);
        //            $pageRenderer->addCssFile($extRealPath . 'Resources/Public/Contrib/BootstrapSelect/css/bootstrap-select.min.css');
        //            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Modules/Backend/Select');
        //            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Backend/Modal');
        //            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Modules/Backend/Modal');
        //
        //        }
        //        parent::initializeView($view);
    }

    /**
     * @param ButtonBar $buttonBar
     * @param string $type
     * @param array $params
     * @param bool $disabled
     * @return ButtonBar
     * @throws Exception
     */
    protected function getButton(ButtonBar $buttonBar, string $type, array $params = [], bool $disabled=false)
    {
        $translationKey = $params['translationKey'] ?? '';
        $table = $params['table'] ?? '';
        $uid = $params['uid'] ?? '';
        $iconIdentifier = $params['iconIdentifier'] ?? '';
        $action = $params['action'] ?? '';
        $controller = $params['controller'] ?? 'Backend';
        $showLabelText = isset($params['showLabelText']);
        //
        // Extension key (normalized)
        $extensionName = $this->extensionName;
        if (substr($extensionName, -3, 3) === 'Pro') {
            $extensionName = substr($extensionName, 0, -3);
        }
        //
        // Title translation
        $translationPrefix = 'tx_' . strtolower($extensionName) . '_label.';
        $translationKey = $translationPrefix . $translationKey;
        $title = $this->translate($translationKey);
        if ($title === null) {
            throw new Exception('Translation key missing: ' . $translationKey);
        }
        //
        // Prepare css classes
        $classes = '';
        if ($disabled) {
            if (isset($_SERVER['DDEV_HOSTNAME'])) {
                $classes = 'bg-danger';
            } else {
                $classes = 'd-none';
            }
        }
        //
        // Build button
        switch ($type) {
            case 'refresh':
                $button = $buttonBar->makeLinkButton()
                    ->setHref(GeneralUtility::getIndpEnv('REQUEST_URI'))
                    ->setTitle($title)
                    ->setClasses($classes)
                    ->setShowLabelText($showLabelText)
                    ->setIcon($this->iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_RIGHT);
                break;
            case 'bookmark':
                $button = $buttonBar->makeShortcutButton()
                    ->setArguments(['id', 'M', $this->modulePrefix])
                    ->setRouteIdentifier($this->moduleName)
                    ->setDisplayName($title);
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_RIGHT);
                break;
            case 'new':
                $parameter = [
                    'returnUrl' => $this->getReturnUrl(),
                    'id' => $this->pageUid,
                    'edit' => [
                        $table => [
                            $this->pageUid => 'new'
                        ]
                    ]
                ];
                $button = $buttonBar->makeLinkButton()
                    ->setHref((string)$this->uriBuilderBackend->buildUriFromRoute('record_edit', $parameter))
                    ->setTitle($title)
                    ->setClasses($classes)
                    ->setShowLabelText($showLabelText)
                    ->setIcon($this->iconFactory->getIcon('actions-document-new', Icon::SIZE_SMALL));
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
                break;
            case 'edit':
                if ($iconIdentifier === '') {
                    $iconIdentifier = 'actions-document-open';
                }
                $parameter = [
                    'returnUrl' => $this->getReturnUrl(),
                    'id' => $this->pageUid,
                    'edit' => [
                        $table => [
                            $uid => 'edit'
                        ]
                    ]
                ];
                $button = $buttonBar->makeLinkButton()
                    ->setHref((string)$this->uriBuilderBackend->buildUriFromRoute('record_edit', $parameter))
                    ->setTitle($title)
                    ->setClasses($classes)
                    ->setShowLabelText($showLabelText)
                    ->setIcon($this->iconFactory->getIcon($iconIdentifier, Icon::SIZE_SMALL));
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
                break;
            case 'csv':
                $parameter = [
                    'id' => $this->pageUid,
                    'action' => $action,
                    'controller' => $controller,
                    'csv' => 1,
                ];
                $uri = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $button = $buttonBar->makeLinkButton()
                    ->setHref($uri)
                    ->setTitle($title)
                    ->setClasses($classes)
                    ->setShowLabelText($showLabelText)
                    ->setIcon($this->iconFactory->getIcon('actions-document-export-csv', Icon::SIZE_SMALL));
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
                break;
            case 'action':
                $parameter = [
                    'id' => $this->pageUid,
                    'action' => $action,
                    'controller' => $controller,
                ];
                $uri = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $button = $buttonBar->makeLinkButton()
                    ->setHref($uri)
                    ->setTitle($title)
                    ->setClasses($classes)
                    ->setShowLabelText($showLabelText)
                    ->setIcon($this->iconFactory->getIcon($iconIdentifier, Icon::SIZE_SMALL));
                $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
                break;
        }
        return $buttonBar;
    }

    protected function setMetaInformation(string $table, int $uid): void
    {
        $metaRecord = BackendUtility::getRecord($table, $uid);
        $this->moduleTemplate->getDocHeaderComponent()->setMetaInformation($metaRecord);
    }

    /**
     * Create action menu
     *
     * @param array<mixed> $actions
     */
    protected function createMenuActions(array $actions)
    {
        $menu = $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
        $menu->setIdentifier($this->moduleName);
        foreach ($actions as $action) {
            $controller = $action['controller'] ?? 'Backend';
            $active = (
                $this->request->getControllerActionName() === $action['action']
                && $this->request->getControllerName() === $controller
            );
            $item = $menu->makeMenuItem()
                ->setTitle($action['label'])
                ->setHref(
                    $this->uriBuilder->reset()->uriFor(
                        $action['action'],
                        [],
                        $controller
                    )
                )
                ->setActive($active);
            $menu->addMenuItem($item);
        }
        $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->addMenu($menu);
    }

    /**
     * @return string
     * @throws RouteNotFoundException
     */
    protected function getReturnUrl(): string
    {
        $parameter = [
            'id' => $this->pageUid,
            'action' => $this->request->getControllerActionName(),
            'controller' => $this->request->getControllerName(),
        ];
        return (string)$this->uriBuilderBackend->buildUriFromRoute($this->moduleName, $parameter);
    }

    protected function getBackendUserAuthentication(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

    protected function translate(string $key, array $arguments = []): string
    {
        $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName);
        $extensionName = $this->extensionName;
        $path = 'EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf';
        $error = 'Translation ' . $key . ' not found in ' . $path;
        if (substr($extensionName, -3, 3) === 'Pro') {
            $extensionName = substr($extensionName, 0, -3);
        }
        $extensionNamePro = $extensionName . 'Pro';
        //
        $translation = LocalizationUtility::translate($key, $extensionName, $arguments);
        if ($translation === null) {
            $translation = LocalizationUtility::translate($key, $extensionNamePro, $arguments);
        }
        if ($translation === null) {
            throw new Exception('Translation ' . $key . ' not found in ' . $path);
        }
        $translation = $translation ?? $error;
        return $translation;
    }

    protected function getIcon(string $key): Icon
    {
        return $this->iconFactory->getIcon($key, IconSize::SMALL);
    }
}
