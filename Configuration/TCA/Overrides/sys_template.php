<?php

defined('TYPO3') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'modules',
    'Configuration/TypoScript',
    'Modules'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'modules',
    'Configuration/TypoScript/Frontend',
    'Modules - FE-Management (third-party modules)'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'modules',
    'Configuration/TypoScript/Stylesheet',
    'Modules - FE-Default stylesheets'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'modules',
    'Configuration/TypoScript/Registration',
    'Modules - FE-Registration & Profile'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'modules',
    'Configuration/TypoScript/Listing',
    'Modules - FE-Listing & Detail'
);
