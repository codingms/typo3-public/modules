<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'modules',
    'Registration',
    'user'
);
\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'modules',
    'Profile',
    'user'
);
\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'modules',
    'Listing',
    'user'
);
