<?php

declare(strict_types=1);

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;
use TYPO3\CMS\Core\Imaging\IconProvider\SvgSpriteIconProvider;

return [
    'apps-pagetree-frontend-user' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/iconmonstr-user-20.svg',
        'sprite' => 'EXT:modules/Resources/Public/Icons/backend-sprites.svg#apps-pagetree-frontend-user',
    ],
    'module-frontenduser' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/module-frontenduser.svg',
    ],
    'module-backenduser' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/module-backenduser.svg',
    ],
    'content-plugin-modules-registration' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/iconmonstr-user-20.svg',
        'sprite' => 'EXT:modules/Resources/Public/Icons/backend-sprites.svg#content-plugin-modules-registration',
    ],
    'content-plugin-modules-profile' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/iconmonstr-user-20.svg',
        'sprite' => 'EXT:modules/Resources/Public/Icons/backend-sprites.svg#content-plugin-modules-profile',
    ],
    'content-plugin-modules-listing' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/iconmonstr-user-20.svg',
        'sprite' => 'EXT:modules/Resources/Public/Icons/backend-sprites.svg#content-plugin-modules-listing',
    ],
    'mimetypes-x-content-modules-invitationcode' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:modules/Resources/Public/Icons/iconmonstr-key-13.svg',
        'sprite' => 'EXT:modules/Resources/Public/Icons/backend-sprites.svg#mimetypes-x-content-modules-invitationcode',
    ],
];
