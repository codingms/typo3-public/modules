<?php

$configuration = \CodingMs\Modules\Utility\ExtensionUtility::getExtensionConfiguration('modules');
$frontendUserModuleEnabled = (bool)($configuration['module']['frontendUser']['disable'] ?? false);
$backendUserModuleEnabled = (bool)($configuration['module']['backendUser']['disable'] ?? false);
$return = [];
//
// Registers a Backend Module
if (!$frontendUserModuleEnabled) {
    $return['modules_frontenduser'] = [
        'parent' => 'web',
        'position' => [],
        'access' => 'user',
        'iconIdentifier' => 'module-frontenduser',
        'path' => '/module/modules/frontenduser',
        'labels' => 'LLL:EXT:modules/Resources/Private/Language/locallang_frontenduser.xlf',
        'extensionName' => 'Modules',
        'controllerActions' => [
            \CodingMs\Modules\Controller\FrontendUserBackendController::class => [
                'list',
                'listFrontendUserGroups',
                'listInvitationCodes',
                'importInvitationCodes'
            ]
        ],
    ];
}
//
// Registers a Backend Module
if (!$backendUserModuleEnabled) {
    $return['modules_backenduser'] = [
        'parent' => 'web',
        'position' => [],
        'access' => 'user',
        'iconIdentifier' => 'module-backenduser',
        'path' => '/module/modules/backenduser',
        'labels' => 'LLL:EXT:modules/Resources/Private/Language/locallang_backenduser.xlf',
        'extensionName' => 'Modules',
        'controllerActions' => [
            \CodingMs\Modules\Controller\BackendUserBackendController::class => [
                'list'
            ]
        ],
    ];
}
return $return;
