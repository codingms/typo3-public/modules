<?php

return [
    'dependencies' => ['core', 'backend'],
    'imports' => [
        '@codingms/modules/' => [
            'path' => 'EXT:modules/Resources/Public/JavaScript/',
            // Exclude files of the following folders from being import-mapped
            //'exclude' => [
            //    'EXT:modules/Resources/Public/JavaScript/Contrib/',
            //],
        ],
    ],
];
