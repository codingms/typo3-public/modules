# TypoScript-Konstanten Einstellungen


## Frontend-Benutzercontainer

| **Konstante**    | themes.configuration.container.frontendUser                                                                    |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Frontend-Benutzer                                                                                              |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |



## Frontend-Benutzerseiten

| **Konstante**    | themes.configuration.pages.registration                                                                        |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Registrierung                                                                                                  |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.profile                                                                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profil                                                                                                         |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.terms                                                                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Bedingungen                                                                                                    |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.login                                                                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Login                                                                                                          |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.privacy                                                                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Datenschutz                                                                                                    |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.disclaimer                                                                          |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Haftungsausschluss                                                                                             |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.frontendUser.list                                                                   |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Seite mit der Liste der Frontend-Benutzer                                                                      |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.pages.frontendUser.detail                                                                 |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Detailseite für Frontend-Benutzer                                                                              |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 0                                                                                                              |



## Fluid Templates für Modules (Anmeldung, Profil, ...)

| **Konstante**    | themes.configuration.extension.modules.view.templateRootPath                                                   |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Templates                                                                                                |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Templates/                                                                       |

| **Konstante**    | themes.configuration.extension.modules.view.partialRootPath                                                    |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Partials                                                                                                 |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Partials/                                                                        |

| **Konstante**    | themes.configuration.extension.modules.view.layoutRootPath                                                     |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Layouts                                                                                                  |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Layouts/                                                                         |

| **Konstante**    | themes.configuration.extension.modules.view.templateRootPath                                                   |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Templates                                                                                                |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Templates/                                                                       |

| **Konstante**    | themes.configuration.extension.modules.view.partialRootPath                                                    |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Partials                                                                                                 |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Partials/                                                                        |

| **Konstante**    | themes.configuration.extension.modules.view.layoutRootPath                                                     |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Fluid Layouts                                                                                                  |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | EXT:modules/Resources/Private/Layouts/                                                                         |



## Registrierung

| **Konstante**    | themes.configuration.extension.modules.registration.emailAsUsername                                            |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Deaktiviert das Benutzernamenfeld und verwendet E-Mail als Benutzernamen                                       |
| **Beschreibung** |                                                                                                                |
| **Typ**          | boolean                                                                                                        |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.extension.modules.registration.usernameMinLength                                          |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Mindestlänge des Registrierungs-Benutzernamens                                                                 |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 6                                                                                                              |

| **Konstante**    | themes.configuration.extension.modules.registration.passwordMinLength                                          |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Mindestlänge des Registrierungspassworts                                                                       |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 10                                                                                                             |

| **Konstante**    | themes.configuration.extension.modules.registration.passwordEncryption                                         |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Passwortverschlüsselung                                                                                        |
| **Beschreibung** | default (verwendet die Systemeinstellung, normalerweise Argon2i), none, md5, sha1 oder saltedpasswords         |
| **Typ**          | string                                                                                                         |
| **Standardwert** | default                                                                                                        |

| **Konstante**    | themes.configuration.extension.modules.registration.enableAdminConfirmation                                    |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Bestätigung aktivieren                                                                                   |
| **Beschreibung** |                                                                                                                |
| **Typ**          | boolean                                                                                                        |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.extension.modules.registration.email.subject                                              |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Registrierungsmail                                                                                             |
| **Beschreibung** | Betreff                                                                                                        |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Registrierung                                                                                                  |

| **Konstante**    | themes.configuration.extension.modules.registration.email.fromEmail                                            |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Registrierungsmail                                                                                             |
| **Beschreibung** | E-Mail des Absenders                                                                                           |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.email.fromName                                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Registrierungsmail                                                                                             |
| **Beschreibung** | Name des Absenders                                                                                             |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Registration                                                                                                   |

| **Konstante**    | themes.configuration.extension.modules.registration.email.bccEmail                                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Registrierungsmail                                                                                             |
| **Beschreibung** | Blindkopie per E-Mail                                                                                          |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailOnActivation.active                                   |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Eine E-Mail bei Aktivierung des Accounts an den Benutzer?                                                      |
| **Beschreibung** |                                                                                                                |
| **Typ**          | boolean                                                                                                        |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.extension.modules.registration.emailOnActivation.subject                                  |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Aktivierungsmail                                                                                               |
| **Beschreibung** | Betreff                                                                                                        |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Activation                                                                                                     |

| **Konstante**    | themes.configuration.extension.modules.registration.emailOnActivation.fromEmail                                |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Aktivierungsmail                                                                                               |
| **Beschreibung** | E-Mail des Absenders                                                                                           |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailOnActivation.fromName                                 |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Aktivierungsmail                                                                                               |
| **Beschreibung** | Name des Absenders                                                                                             |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Registration                                                                                                   |

| **Konstante**    | themes.configuration.extension.modules.registration.emailOnActivation.bccEmail                                 |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Aktivierungsmail Blindkopie-E-Mail                                                                             |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminActivation.subject                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Aktivierungsmail                                                                                         |
| **Beschreibung** | Betreff                                                                                                        |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Activation                                                                                                     |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminActivation.fromEmail                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Aktivierungsmail                                                                                         |
| **Beschreibung** | E-Mail-Absender                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminActivation.toEmail                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Aktivierungsmail                                                                                         |
| **Beschreibung** | Empfänger-E-Mail                                                                                               |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminActivation.bccEmail                              |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Aktivierungsmail                                                                                         |
| **Beschreibung** | Blindkopie-E-Mail                                                                                              |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminConfirmation.subject                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Bestätigungsmail                                                                                         |
| **Beschreibung** | Betreff                                                                                                        |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Activation Confirmation                                                                                        |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminConfirmation.fromEmail                           |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Bestätigungsmail                                                                                         |
| **Beschreibung** | Absender                                                                                                       |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminConfirmation.fromName                            |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Bestätigungsmail                                                                                         |
| **Beschreibung** | Name des Absenders                                                                                             |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Registration                                                                                                   |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminConfirmation.bccEmail                            |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Bestätigungsmail                                                                                         |
| **Beschreibung** | Blindkopie-E-Mail                                                                                              |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminRejection.subject                                |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Ablehnungsmail                                                                                           |
| **Beschreibung** | Betreff                                                                                                        |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Activation Rejection                                                                                           |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminRejection.fromEmail                              |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Ablehnungsmail                                                                                           |
| **Beschreibung** | E-Mail-Absender                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminRejection.fromName                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Ablehnungsmail                                                                                           |
| **Beschreibung** | Absendername                                                                                                   |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Registration                                                                                                   |

| **Konstante**    | themes.configuration.extension.modules.registration.emailAdminRejection.bccEmail                               |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Admin-Ablehnungsmail                                                                                           |
| **Beschreibung** | Blindkopie-E-Mail                                                                                              |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.frontendUserGroups                                         |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Frontend-Benutzergruppen für neu registrierte Benutzer                                                         |
| **Beschreibung** | Mehrere Gruppen müssen durch Komma getrennt werden. Gruppen müssen im frontendUser-Container enthalten sein!   |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.recordType                                                 |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Datensatztyp des Front-End-Benutzers                                                                           |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.registration.professions                                                |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Beruf                                                                                                          |
| **Beschreibung** |                                                                                                                |
| **Typ**          | text                                                                                                           |
| **Standardwert** | employed,officer,self_employed,pensioner                                                                       |

| **Konstante**    | themes.configuration.extension.modules.registration.maritalStatus                                              |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Familienstand                                                                                                  |
| **Beschreibung** |                                                                                                                |
| **Typ**          | text                                                                                                           |
| **Standardwert** | single,married                                                                                                 |

| **Konstante**    | themes.configuration.extension.modules.registration.gender                                                     |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Geschlecht                                                                                                     |
| **Beschreibung** |                                                                                                                |
| **Typ**          | text                                                                                                           |
| **Standardwert** | male,female                                                                                                    |

| **Konstante**    | themes.configuration.extension.modules.registration.accountingType                                             |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Konto Typ                                                                                                      |
| **Beschreibung** |                                                                                                                |
| **Typ**          | text                                                                                                           |
| **Standardwert** | accounting,debit                                                                                               |

| **Konstante**    | themes.configuration.extension.modules.registration.minAge                                                     |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Der Benutzer muss mindestens folgendes Alter haben                                                             |
| **Beschreibung** |                                                                                                                |
| **Typ**          | int+                                                                                                           |
| **Standardwert** | 18                                                                                                             |

| **Konstante**    | themes.configuration.extension.modules.registration.invitationRequired                                         |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | User must have a valid invitation code for registration                                                        |
| **Beschreibung** |                                                                                                                |
| **Typ**          | boolean                                                                                                        |
| **Standardwert** | 0                                                                                                              |



## Profil

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.active                                        |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Eine E-Mail zu Profilaktualisierungen durch den Benutzer senden?                                               |
| **Beschreibung** |                                                                                                                |
| **Typ**          | boolean                                                                                                        |
| **Standardwert** | 0                                                                                                              |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.subject                                       |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Betreff der Profilaktualisierungsmail                                                                          |
| **Beschreibung** |                                                                                                                |
| **Typ**          | string                                                                                                         |
| **Standardwert** | Profile update                                                                                                 |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.fromEmail                                     |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profilaktualisierungs-E-Mail                                                                                   |
| **Beschreibung** | Absender-Mail                                                                                                  |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.fromName                                      |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profilaktualisierungsmail                                                                                      |
| **Beschreibung** | Absendername                                                                                                   |
| **Typ**          | string                                                                                                         |
| **Standardwert** | profile                                                                                                        |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.toEmail                                       |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profil-Update-E-Mail                                                                                           |
| **Beschreibung** | Empfänger-Mail                                                                                                 |
| **Typ**          | string                                                                                                         |
| **Standardwert** | mail@domain.com                                                                                                |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.toName                                        |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profilaktualisierung                                                                                           |
| **Beschreibung** | Name des E-Mail-Empfängers                                                                                     |
| **Typ**          | string                                                                                                         |
| **Standardwert** | profile                                                                                                        |

| **Konstante**    | themes.configuration.extension.modules.profile.emailOnUserUpdate.bccEmail                                      |
|:-----------------|:---------------------------------------------------------------------------------------------------------------|
| **Label**        | Profil-Update-Mail                                                                                             |
| **Beschreibung** | Blindkopie-E-Mail                                                                                              |
| **Typ**          | string                                                                                                         |
| **Standardwert** |                                                                                                                |



