# PSR-14 Events

## AfterProfileUpdatedEvent

Dieses Event wird ausgelöst, nachdem ein Frontend-Nutzer sein Profil manuell, durch Versenden des Formulars auf der
Profilseite, bearbeitet hat. Das Event-Objekt enthält den Frontend-Nutzer-Datensatz.

Sie können einen Event-Listener für dieses Event wie folgt konfigurieren:

- Ergänzen sie `Configuration/Services.yaml`
```yaml
services:
  Vendor\MyExtension\EventListener\AfterProfileUpdatedEventListener:
    tags:
      - name: event.listener
        identifier: 'AfterProfileUpdatedEventListener'
        event: CodingMs\Modules\Event\Profile\AfterProfileUpdatedEvent
```
- Event-Listener Klasse erstellen
```php
namespace Vendor\MyExtension\EventListener;
use CodingMs\Modules\Event\Profile\AfterProfileUpdatedEvent;
class AfterProfileUpdatedEventListener
{
    public function __invoke(AfterProfileUpdatedEvent $event): void
    {

    }
}
```

Mehr Informationen zu PSR-14 Events finden Sie hier: https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Events/EventDispatcher/Index.html
