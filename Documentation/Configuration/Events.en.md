# PSR-14 Events

## AfterProfileUpdatedEvent

This event is triggered after a frontend user has manually edited his profile by sending the form on the
profile page. The event object contains the frontend user record.

You can configure an event listener for this event as follows:

- Add the following to `Configuration/Services.yaml`
```yaml
services:
  Vendor\MyExtension\EventListener\AfterProfileUpdatedEventListener:
    tags:
      - name: event.listener
        identifier: 'AfterProfileUpdatedEventListener'
        event: CodingMs\Modules\Event\Profile\AfterProfileUpdatedEvent
```
- Create event listener class
```php
namespace Vendor\MyExtension\EventListener;
use CodingMs\Modules\Event\Profile\AfterProfileUpdatedEvent;
class AfterProfileUpdatedEventListener
{
    public function __invoke(AfterProfileUpdatedEvent $event): void
    {

    }
}
```

You can find more information about PSR-14 events here: https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Events/EventDispatcher/Index.html
