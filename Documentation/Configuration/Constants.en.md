# TypoScript constant settings


## Frontend user Container

| **Constant**      | themes.configuration.container.frontendUser                                                  |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Frontend user                                                                                |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |



## Frontend user Pages

| **Constant**      | themes.configuration.pages.registration                                                      |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration                                                                                 |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.profile                                                           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile                                                                                      |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.terms                                                             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Terms                                                                                        |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.login                                                             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Login                                                                                        |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.privacy                                                           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Privacy protection                                                                           |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.disclaimer                                                        |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Disclaimer                                                                                   |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |



## Fluid Templates for modules (Registration, Profile, ...)

| **Constant**      | themes.configuration.extension.modules.view.templateRootPath                                 |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Templates                                                                              |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Templates/                                                     |

| **Constant**      | themes.configuration.extension.modules.view.partialRootPath                                  |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Partials                                                                               |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Partials/                                                      |

| **Constant**      | themes.configuration.extension.modules.view.layoutRootPath                                   |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Layouts                                                                                |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Layouts/                                                       |

| **Constant**      | themes.configuration.extension.modules.view.templateRootPath                                 |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Templates                                                                              |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Templates/                                                     |

| **Constant**      | themes.configuration.extension.modules.view.partialRootPath                                  |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Partials                                                                               |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Partials/                                                      |

| **Constant**      | themes.configuration.extension.modules.view.layoutRootPath                                   |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Fluid Layouts                                                                                |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | EXT:modules/Resources/Private/Layouts/                                                       |



## Registration

| **Constant**      | themes.configuration.extension.modules.registration.emailAsUsername                          |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Disables the username field and use email as username                                        |
| **Description**   |                                                                                              |
| **Type**          | boolean                                                                                      |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.extension.modules.registration.usernameMinLength                        |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration username minimum length                                                         |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 6                                                                                            |

| **Constant**      | themes.configuration.extension.modules.registration.passwordMinLength                        |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration password minimum length                                                         |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 10                                                                                           |

| **Constant**      | themes.configuration.extension.modules.registration.passwordEncryption                       |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Password encryption                                                                          |
| **Description**   | default (uses the system setting, usually Argon2i), none, md5, sha1 or saltedpasswords       |
| **Type**          | string                                                                                       |
| **Default value** | default                                                                                      |

| **Constant**      | themes.configuration.extension.modules.registration.enableAdminConfirmation                  |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Enable admin confirmation                                                                    |
| **Description**   |                                                                                              |
| **Type**          | boolean                                                                                      |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.extension.modules.registration.email.subject                            |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration mail subject                                                                    |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Registrierung                                                                                |

| **Constant**      | themes.configuration.extension.modules.registration.email.fromEmail                          |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration mail sender mail                                                                |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.email.fromName                           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration mail sender name                                                                |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Registration                                                                                 |

| **Constant**      | themes.configuration.extension.modules.registration.email.bccEmail                           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Registration mail blind copy email                                                           |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailOnActivation.active                 |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Send an email on account activation to the user?                                             |
| **Description**   |                                                                                              |
| **Type**          | boolean                                                                                      |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.extension.modules.registration.emailOnActivation.subject                |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Activation mail subject                                                                      |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Activation                                                                                   |

| **Constant**      | themes.configuration.extension.modules.registration.emailOnActivation.fromEmail              |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Activation mail sender mail                                                                  |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailOnActivation.fromName               |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Activation mail sender name                                                                  |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Registration                                                                                 |

| **Constant**      | themes.configuration.extension.modules.registration.emailOnActivation.bccEmail               |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Activation mail blind copy email                                                             |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminActivation.subject             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin activation mail subject                                                                |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Activation                                                                                   |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminActivation.fromEmail           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin activation mail sender mail                                                            |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminActivation.toEmail             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin activation mail receiver mail                                                          |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminActivation.bccEmail            |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin activation mail blind copy email                                                       |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminConfirmation.subject           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin confirmation mail subject                                                              |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Activation Confirmation                                                                      |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminConfirmation.fromEmail         |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin confirmation mail sender mail                                                          |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminConfirmation.fromName          |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin confirmation mail sender name                                                          |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Registration                                                                                 |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminConfirmation.bccEmail          |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin confirmation mail blind copy email                                                     |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminRejection.subject              |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin rejection mail subject                                                                 |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Activation Rejection                                                                         |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminRejection.fromEmail            |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin rejection mail sender mail                                                             |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminRejection.fromName             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin rejection mail sender name                                                             |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Registration                                                                                 |

| **Constant**      | themes.configuration.extension.modules.registration.emailAdminRejection.bccEmail             |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Admin rejection mail blind copy email                                                        |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.frontendUserGroups                       |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Frontend user groups for new registered users                                                |
| **Description**   | Multiple groups must be comma separated. Groups must be contained in frontendUser-Container! |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.recordType                               |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Frontend user record type                                                                    |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |

| **Constant**      | themes.configuration.extension.modules.registration.professions                              |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profession                                                                                   |
| **Description**   |                                                                                              |
| **Type**          | text                                                                                         |
| **Default value** | employed,officer,self_employed,pensioner                                                     |

| **Constant**      | themes.configuration.extension.modules.registration.maritalStatus                            |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Marital status                                                                               |
| **Description**   |                                                                                              |
| **Type**          | text                                                                                         |
| **Default value** | single,married                                                                               |

| **Constant**      | themes.configuration.extension.modules.registration.gender                                   |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Gender                                                                                       |
| **Description**   |                                                                                              |
| **Type**          | text                                                                                         |
| **Default value** | male,female                                                                                  |

| **Constant**      | themes.configuration.extension.modules.registration.accountingType                           |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Account type                                                                                 |
| **Description**   |                                                                                              |
| **Type**          | text                                                                                         |
| **Default value** | accounting,debit                                                                             |

| **Constant**      | themes.configuration.extension.modules.registration.minAge                                   |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | User must have at least an age of                                                            |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 18                                                                                           |

| **Constant**      | themes.configuration.extension.modules.registration.invitationRequired                       |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | User must have a valid invitation code for registration                                      |
| **Description**   |                                                                                              |
| **Type**          | boolean                                                                                      |
| **Default value** | 0                                                                                            |



## Profile

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.active                      |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Send an email on profile updates by user?                                                    |
| **Description**   |                                                                                              |
| **Type**          | boolean                                                                                      |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.subject                     |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail subject                                                                  |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | Profile update                                                                               |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.fromEmail                   |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail sender mail                                                              |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.fromName                    |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail sender name                                                              |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | profile                                                                                      |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.toEmail                     |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail receiver mail                                                            |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | mail@domain.com                                                                              |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.toName                      |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail receiver name                                                            |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** | profile                                                                                      |

| **Constant**      | themes.configuration.extension.modules.profile.emailOnUserUpdate.bccEmail                    |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Profile update mail blind copy email                                                         |
| **Description**   |                                                                                              |
| **Type**          | string                                                                                       |
| **Default value** |                                                                                              |



## Frontend user pages

| **Constant**      | themes.configuration.pages.frontendUser.list                                                 |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Frontend users list page                                                                     |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |

| **Constant**      | themes.configuration.pages.frontendUser.detail                                               |
| :---------------- | :------------------------------------------------------------------------------------------- |
| **Label**         | Frontend users details page                                                                  |
| **Description**   |                                                                                              |
| **Type**          | int+                                                                                         |
| **Default value** | 0                                                                                            |



